/**
 * BLOCK: gutenberg-blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {PlainText, MediaUpload, MediaUploadCheck} = wp.editor;

registerBlockType( 'cgb/banner-block-gutenberg-blocks', {
	title: __( 'Banner Block' ),
	icon: {
		background: '#6866d1',
		foreground: '#FFFFFF',
		src: 'text'
	},
	category: 'common',
	keywords: [
		__( 'gutenberg-blocks — CGB Block' ),
		__( 'CGB Block Banner' ),
		__( 'create-guten-block-banner' ),
	],
	attributes: {
		title: {type: 'string', default: ''},
		paragraph: {type: 'string', default: ''},
	},
	edit: ( {className, attributes, setAttributes} ) => {
		return (
			<div className={ className }>
				<div className={'banner-block'}>
					<PlainText className={'banner-block__title'}
							   placeholder={'Please, enter the title'}
							   onChange = {content => {
									setAttributes({ title: content })
							   }}
							   value={attributes.title}/>
					<PlainText className={'banner-block__paragraph'}
							   placeholder={'Please, enter the text'}
							   onChange = {content => {
								   setAttributes({ paragraph: content })
							   }}
							   value={attributes.paragraph}/>
				</div>
			</div>
		);
	},
	save: ( {className, attributes} ) => {
		return (
			<div className={ className }>
				<div className={'banner-block'}>
					<h4 className={'banner-block__title'}>{attributes.title}</h4>
					<p className={'banner-block__paragraph'}>{attributes.paragraph}</p>
				</div>
			</div>
		);

	},
} );
