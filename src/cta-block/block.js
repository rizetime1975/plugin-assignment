/**
 * BLOCK: gutenberg-blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {PlainText, MediaUpload, MediaUploadCheck} = wp.editor;

registerBlockType( 'cgb/cta-block-gutenberg-blocks', {
	title: __( 'CTA Block' ),
	icon: {
		background: '#fe515d',
		foreground: '#FFFFFF',
		src: 'text'
	},
	category: 'common',
	keywords: [
		__( 'gutenberg-blocks — CTA Block' ),
		__( 'CGB Block CTA' ),
		__( 'create-guten-block-cta' ),
	],
	attributes: {
		link: {type: 'string', default: ''},
		paragraph: {type: 'string', default: ''},
	},
	edit: ( {className, attributes, setAttributes} ) => {
		return (
			<div className={ className }>
				<div className={'cta-block'}>
					<PlainText className={'cta-block__link'}
							   placeholder={'Please, enter the link'}
							   onChange = {content => {
								   setAttributes({ link: content })
							   }}
							   value={attributes.link}/>
					<PlainText className={'cta-block__paragraph'}
							   placeholder={'Please, enter the text'}
							   onChange = {content => {
								   setAttributes({ paragraph: content })
							   }}
							   value={attributes.paragraph}/>
				</div>
			</div>
		);
	},
	save: ( {className, attributes} ) => {
		return (
			<div className={ className }>
				<div className={'cta-block'}>
					<div className={'cta-block__paragraph'}>{attributes.paragraph}</div>
					<a href={attributes.link} className={'cta-block__link'}>Visit Site</a>
				</div>
			</div>
		);

	},
} );
